from django.urls import path

from . import views

app_name = 'masquerade'
urlpatterns = [
    path('mask/', views.mask, name='mask'),
    path('unmask/', views.unmask, name='unmask'),
]
