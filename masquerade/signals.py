from django.dispatch import Signal

# Providing arguments 'mask_username'
mask_on = Signal()
mask_off = Signal()
